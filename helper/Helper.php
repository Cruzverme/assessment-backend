<?php
    namespace app\storeManager\helper;

    class Helper {
      public static function logger($message){
        
        $ip = $_SERVER['REMOTE_ADDR']; // Get IP 
        $hour = date('Y-m-d H:i:s'); // Get Datetime in MYSQL format

        #$message = $mysqli->real_escape_string($message); // get Message and Avoid String Problems

        try{
          database()
            ->set('message=?',$message)
            ->set('hour=?',$hour)
            ->set('ip=?',$ip)
            ->from('logs')
            ->insert();
          return true;
        } catch(\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to insert. '.database()->lastError(),'error');
        }
        
      }
    }
