<?php 

  require_once("db_config.php");
  
  $createTableProduct = "CREATE TABLE IF NOT EXISTS products ( id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                          name varchar(255), SKU varchar(255), product_image longblob,
                          price double NOT NULL, description varchar(255), quantity int)";

  $createTableCategory = "CREATE TABLE IF NOT EXISTS categories ( code int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                          name varchar(255))";

  $createTableProductCategory = "CREATE TABLE IF NOT EXISTS product_category ( id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                                product_id int UNSIGNED NOT NULL, 
                                category_id int UNSIGNED NOT NULL,
                                FOREIGN KEY (category_id) REFERENCES categories(code),
                                FOREIGN KEY (product_id) REFERENCES products(id) )";

  $createLogTable = "CREATE TABLE IF NOT EXISTS logs ( id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                      hour datetime NOT NULL,
                      ip varchar(128) NOT NULL,
                      message text NOT NULL,
                      KEY hour (hour)) ";

  //Create the Tables with Querys Above
  if($connection->query($createTableProduct))
    echo "Product Table Created in Database!" . PHP_EOL;
  else
    echo "Product Table Not Created in Database!". $connection->error . PHP_EOL;

  if($connection->query($createTableCategory))
    echo "Category Table Created in Database!" . PHP_EOL;
  else
    echo "Category Table Not Created in Database!". $connection->error . PHP_EOL;

  if($connection->query($createTableProductCategory))
    echo "Product Category Table Created in Database!" . PHP_EOL;
  else
    echo "Product Category Table Not Created in Database!". $connection->error . PHP_EOL;

  if($connection->query($createLogTable))
    echo "Log Table Created in Database!" . PHP_EOL;
  else
    echo "Log Table Not Created in Database!". $connection->error . PHP_EOL;

  //Close the DB Connection
  $connection->close();

?>