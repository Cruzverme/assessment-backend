<?php
  
  include "app.php";
 
  require_once $base.'configs\db_credentials.php';
  
  $db_user = DbCredentials::DB_USER;
  $db_password = DbCredentials::DB_PASSWORD;
  $db_name = DbCredentials::DB_NAME;
  $db_host = DbCredentials::DB_SERVER;

  $connection = new mysqli($db_host, $db_user, $db_password, $db_name);

  if($connection->connect_error)
  {
    die("Sorry a error occours, connection failed: " . $connection->connect_error);
  }
?>