<?php 
  include "app.php";

  use app\storeManager\dao\ProductDao;
  use app\storeManager\dao\CategoryDao;
  use app\storeManager\dao\ProductCategoryDao;
  
  $file = $base.'optionals/import.csv';
  $delimiter = ';';
  $row = 1;
  if (($handle = fopen("$file", "r")) !== FALSE) {
    $totalCategory = 0;
    $totalProducts = 0;
    $totalProductCategory = 0;  

    echo "######### Starting Importations ######### \n\r";
    while (($data = fgetcsv($handle, 0, "$delimiter")) !== FALSE) {
      $num = count($data);
      if($data[0] == 'nome')
        continue;
      
      $products = array('name'=>$data[0], 'SKU'=>$data[1], 'description'=>$data[2], 'quantity'=>$data[3], 'price'=>$data[4]);
      
      // $product_exists = ProductDao::listProductName($products['name']);
      // if($product_exists == null && strlen($products['name']) > 0)
      // {
        if(ProductDao::insert($products))
          $totalProducts+=1;
      // }
      
      $categoryList = explode('|',$data[5]);
      
      $lastProduct = ProductDao::listLast();     
      
      ### Categories in file filter with a foreach
      foreach($categoryList as $categoryName)
      {
        $category = array('name'=>$categoryName);
        $category_exists = CategoryDao::listCategoryName($categoryName);
        
        
        if($category_exists == null && strlen($categoryName) > 0) //if category don't exists in DB, will add
        {
          CategoryDao::insert($category);
          $category_exists_code = CategoryDao::listLast(); //if category dont exists in DB, will get the last Category
          $totalCategory+=1;
        }elseif(strlen($categoryName) > 0){
          $category_exists_code = CategoryDao::listCategoryName($category_exists['name']); // get Category Code
        }else{
          continue;
        }
        
        if($category_exists_code){
          ProductCategoryDao::insert($lastProduct,$category_exists_code['code']); //insert product with category
          $totalProductCategory+=1;
        }
      }
      
      $row++;
    }
    echo "$totalProducts Products was added and $totalCategory Category Was Added and $totalProductCategory associations";
    fclose($handle);
  }


