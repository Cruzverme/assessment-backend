<?php 

  namespace app\storeManager\dao;

  class ProductCategoryDao
  {
    public static function insert($product,$category) {
      try{
        database()
          ->set('product_id=?',$product['id'])
          ->set('category_id=?',$category)
          ->from('product_category')
          ->insert();
        return true;
      } catch(Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to insert. '.database()->lastError(),'error');
      }
    }

    public static function update($product,$category) {
      try{
        database()
          ->from('product_category')
          ->set('category_id=?',$category)
          ->where('product_id=?',$product['id'])
          ->update();
        return true;
      } catch(Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to insert. '.database()->lastError(),'error');
      }
    }

    public static function listProductCategories($productID){
      try{
        $sql = "SELECT category_id as code
                FROM product_category prodCat 
                inner join categories on prodCat.category_id = categories.code
                INNER JOIN products ON prodCat.product_id = products.id
                WHERE product_id=$productID";
        
        $products = database()->runRawQuery($sql);
        
        
      } catch (\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
          $products=[];
      }
      return $products;
    }

    public static function listByCategory($productID) {
      try {
        
        $sql = "SELECT prodCat.product_id,products.name, products.SKU, products.product_image, 
                  products.description, products.price, products.quantity,categories.name as category
                FROM product_category prodCat 
                inner join categories on prodCat.category_id = categories.code
                INNER JOIN products ON prodCat.product_id = products.id
                WHERE product_id=$productID";
        
        $products = database()->runRawQuery($sql);
        
        
      } catch (\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
          $products=[];
      }
      return $products;
    }

    public static function list() {
      try {
          $products = database()->select('*')
              ->from('product_category')
              ->order('id desc')
              ->limit('0,20')
              ->toList(); // select * from products
      } catch (\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
          $products=[];
      }
      return $products;
    }
    
    ### REMOVE PRODUCT ###
    public static function delete($id){
      try{
        database()
          ->from('product_category')
          ->where('product_id=?',$id)
          ->delete();
        return true;
      } catch (\Exception $e) {
        valid()->addMessage('ERRORDELETE','Unable to delete. '.database()->lastError(),'error');
      }
    }

    public static function deleteByCategory($id){
      try{
        database()
          ->from('product_category')
          ->where('category_id=?',$id)
          ->delete();
        return true;
      } catch (\Exception $e) {
        valid()->addMessage('ERRORDELETE','Unable to delete. '.database()->lastError(),'error');
      }
    }


  }