<?php 

  namespace app\storeManager\dao;

  class CategoryDao
  {
    public static function insert($category) {
      try{
        database()
          ->set('name=?',$category['name'])
          ->from('categories')
          ->insert();
        return true;
      } catch(\Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to insert. '.database()->lastError(),'error');
      }
    }

    public static function listOne($id) {
      try {
        $category = database()->select('*')
          ->from('categories')
          ->where('code=?',$id)
          ->first(); // select * from categories where $id
      } catch (\Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
        $category=[];
      }
      return $category;
    }

    public static function listLast() {
      try {
          $category = database()->select('*')
              ->from('categories')
              ->order('code desc')
              ->first(); // select * from categories
      } catch (\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
          $category=[];
      }
      return $category ;
    }

    public static function listCategoryName($name){
      try {
        $category = database()->select('*')
          ->from('categories')
          ->where('name=?',$name)
          ->first();
      } catch (\Throwable $th) {
        valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
        $category=[];
      }
      return $category;
    }

    public static function listMoreOne($id) {
      try {
        $category = database()->select('*')
          ->from('categories')
          ->where('code=?',$id)
          ->first(); // select * from categories where $id
      } catch (\Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
        $category=[];
      }
      return $category;
    }

    public static function list() {
      try {
          $categories = database()->select('*')
              ->from('categories')
              ->order('code desc')
              ->toList(); // select * from categories
      } catch (\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
          $categories=[];
      }
      return $categories;
    }
    
    public static function update($category){
      try {
        database()
          ->from('categories')
          ->set('name=?',$category['name'])
          ->where('code=?',$category['code'])
          ->update();
        return true;
      } catch (\Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to Update. '.database()->lastError(),'error');
      }
    }

    public static function delete($id){
      try{
        database()
          ->from('categories')
          ->where('code=?',$id)
          ->delete();
        return true;
      } catch (\Exception $e) {
        valid()->addMessage('ERRORDELETE','Unable to delete. '.database()->lastError(),'error');
      }
    }
  }