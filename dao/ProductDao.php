<?php 

  namespace app\storeManager\dao;

  class ProductDao
  {
    public static function insert($product) {
      try{
        database()
          ->set('name=?',$product['name'])
          ->set('SKU=?',$product['SKU'])
          ->set('description=?',$product['description'])
          ->set('price=?',$product['price'])
          ->set('quantity=?',$product['quantity'])
          ->from('products')
          ->insert();
        return true;
      } catch(Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to insert. '.database()->lastError(),'error');
      }
    }

    public static function update($product) {
      try{
        database()
          ->from('products')
          ->set('name=?',$product['name'])
          ->set('SKU=?',$product['SKU'])
          ->set('description=?',$product['description'])
          ->set('price=?',$product['price'])
          ->set('quantity=?',$product['quantity'])
          ->where('id=?',$product['id'])
          ->update();
        return true;
      } catch(Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to update. '.database()->lastError(),'error');
      }
    }

    public static function listOne($id) {
      try {
        $products = database()->select('*')
          ->from('products')
          ->where('id=?',$id)
          ->first(); // select * from products where $id
      } catch (\Exception $e) {
        valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
        $products=[];
      }
      return $products;
    }

    public static function list() {
      try {
          $products = database()->select('*')
              ->from('Products')
              ->order('id desc')
              ->toList(); // select * from products
      } catch (\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
          $products=[];
      }
      return $products;
    }

    public static function listProductName($name){
      try {
        $products = database()->select('name')
          ->from('products')
          ->where('name=?',$name)
          ->first();
      } catch (\Throwable $th) {
        valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
        $products=[];
      }
      return $products;
    }

    public static function listLast() {
      try {
          $products = database()->select('*')
              ->from('products')
              ->order('id desc')
              ->first(); // select * from products
      } catch (\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
          $products=[];
      }
      return $products ;
    }

    public static function listOnly4() {
      try {
          $products = database()->select('*')
              ->from('products')
              ->order('id desc')
              ->limit('0,4')
              ->toList(); // select * from products
      } catch (\Exception $e) {
          valid()->addMessage('ERRORINSERT','Unable to list. '.database()->lastError(),'error');
          $products=[];
      }
      return $products ;
    }


### REMOVE PRODUCT ###
    public static function delete($id){
      try{
        database()
          ->from('products')
          ->where('id=?',$id)
          ->delete();
        return true;
      } catch (\Exception $e) {
        valid()->addMessage('ERRORDELETE','Unable to delete. '.database()->lastError(),'error');
      }
    }
  }