<?php 

  namespace app\storeManager\factory;

  class ProductFactory{
    
    public static function Factory() {
      return ['id'=>0,'name'=>'','SKU'=>'','product_image', 'description'=>'','price'=>0.0,'quantity'=>0];
    }

    public static function Fetch() {
      $product=self::Factory(); // we star creating an empty product
      $product['id']=Valid()
        ->type('integer')
        ->required(false)
        ->def(0)
        ->condition('gte','The id can\'t be negative',0)
        ->fetch(INPUT_POST,'id');        
      $product['name']=Valid()
        ->type('varchar')
        ->required(true)
        ->condition('maxlen','The name must not have than 50 characters',50)
        ->condition('minlen','The name must have at least 3 characters',3)
        ->fetch(INPUT_POST,'name');
      $product['product_image']=Valid()
        ->type('blob')
        ->required(false)
        ->fetch(INPUT_POST,'product_image');
      $product['SKU']=Valid()
        ->type('varchar')
        ->required(true)
        ->condition('maxlen','The sku must not have more than 50 characters',50)
        ->condition('minlen','The sku must have at least 3 characters',3)
        ->fetch(INPUT_POST,'SKU');
      $product['description']=Valid()
        ->type('varchar')
        ->required(true)
        ->condition('maxlen','The description not have more than 200 characters',200)
        ->condition('minlen','The description must have at least 3 characters',3)
        ->fetch(INPUT_POST,'description');
      $product['price']=Valid()
        ->type('decimal')
        ->def(0.0)
        ->condition('gte','The price can\'t be negative', 0.0)
        ->ifFailThenDefault()
        ->fetch(INPUT_POST,'price');
      $product['quantity']=Valid()
        ->type('integer')
        ->def(0)
        ->condition('gte','The quantity can\'t be negative', 0)
        ->ifFailThenDefault()
        ->fetch(INPUT_POST,'quantity');
      $product['categories[]']=Valid()
        ->type('array')
        ->required(true)
        ->fetch(INPUT_POST,'categories');
      
      return $product;
    }

    #### CREATED TO GET VALUES IN UPDATE FORM

    public static function FetchUpdate() {
      $product=self::Factory(); // we star creating an empty product
      $product['id']=Valid()
        ->type('integer')
        ->required(false)
        ->def(0)
        ->condition('gte','The id can\'t be negative',0)
        ->fetch(INPUT_GET,'id');        
      $product['name']=Valid()
        ->type('varchar')
        ->required(true)
        ->condition('maxlen','The name must not have than 50 characters',50)
        ->condition('minlen','The name must have at least 3 characters',3)
        ->fetch(INPUT_GET,'name');
      $product['product_image']=Valid()
        ->type('blob')
        ->required(true)
        ->fetch(INPUT_GET,'product_image');
      $product['SKU']=Valid()
        ->type('varchar')
        ->required(true)
        ->condition('maxlen','The sku must not have more than 50 characters',50)
        ->condition('minlen','The sku must have at least 3 characters',3)
        ->fetch(INPUT_GET,'SKU');
      $product['description']=Valid()
        ->type('varchar')
        ->required(true)
        ->condition('maxlen','The description not have more than 200 characters',200)
        ->condition('minlen','The description must have at least 3 characters',3)
        ->fetch(INPUT_GET,'description');
      $product['price']=Valid()
        ->type('decimal')
        ->def(0.0)
        ->condition('gte','The price can\'t be negative', 0.0)
        ->ifFailThenDefault()
        ->fetch(INPUT_GET,'price');
      $product['quantity']=Valid()
        ->type('integer')
        ->def(0)
        ->condition('gte','The quantity can\'t be negative', 0)
        ->ifFailThenDefault()
        ->fetch(INPUT_GET,'quantity');
      $product['categories[]']=Valid()
        ->type('array')
        ->required(true)
        ->fetch(INPUT_GET,'categories');
      
      return $product;
    }
  }