<?php 

  namespace app\storeManager\factory;

  class CategoryFactory{
    
    public static function Factory() {
      return ['code'=>0,'name'=>''];
    }

    public static function Fetch() {
      $category=self::Factory(); // we star creating an empty category
      $category['code']=Valid()
        ->type('integer')
        ->required(false)
        ->def(0)
        ->condition('gte','The id can\'t be negative',0)
        ->fetch(INPUT_POST,'code');        
      $category['name']=Valid()
        ->type('varchar')
        ->required(true)
        ->condition('maxlen','The name must not have than 255 characters',255)
        ->fetch(INPUT_POST,'name');
      return $category;
    }

    #### CREATED TO GET VALUES IN UPDATE FORM
    public static function FetchUpdate() {
      $category=self::Factory(); // we star creating an empty category
      $category['code']=Valid()
        ->type('integer')
        ->required(false)
        ->def(0)
        ->condition('gte','The id can\'t be negative',0)
        ->fetch(INPUT_GET,'code');        
      $category['name']=Valid()
        ->type('varchar')
        ->required(true)
        ->condition('maxlen','The name must not have than 255 characters',255)
        ->fetch(INPUT_GET,'name');
      return $category;
    }
  }