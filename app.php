<?php 

  use eftec\bladeone\BladeOne;
  use eftec\PdoOne;
  use eftec\routeone\RouteOne;
  use eftec\ValidationOne;

  $base = __DIR__ . '/';

  include $base."vendor/autoload.php";
  
  require_once $base."configs/db_credentials.php";
  
  // validation
  // Again, our container :-3
  function valid()  {
    global $valid;
    if ($valid===null) {
        $valid=new ValidationOne();
    }
    return $valid;
  }
  // persistence

  // Our container.
  function database() {
    global $database;
    if ($database===null) {
        $db_user = DbCredentials::DB_USER;
        $db_password = DbCredentials::DB_PASSWORD;
        $db_name = DbCredentials::DB_NAME;
        $db_host = DbCredentials::DB_SERVER;
    
        $database=new PdoOne('mysql',$db_host,$db_user,$db_password,$db_name);
        $database->logLevel=3; // it shows all errors
        try {
            $database->connect(true);
        } catch(exception $ex) {
            die("The database is not available");
        }
    }
    return $database;
  }

  // view
  // our container. :-3 (it's a global function that is also a singleton
  function blade() {
    global $blade;
    if ($blade===null) {
        $blade=new BladeOne();
        $blade->setMode(BladeOne::MODE_DEBUG);
    }
    return $blade;
  }

  // route
  // our container. :-3
  function router() {
    global $router;
    
    if ($router===null) {
        $router=new RouteOne();
        $router->setDefaultValues('Application','Home'); // the default controller is Application and the default action is Index.
        $router->fetch(); // we process the current route.
    }
    
    return $router;
  }
?>