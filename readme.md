# Versões
- Xampp - 3.2.4
- PHP - 7.3.10
- Zend Engine - 3.3.10
- MariaDB - 10.4.8
- APACHE - 2.4.41
- Composer - 1.9.0

# Biblioteca Utilizadas
- bladeone - Facilita a View
- pdoone - Facilita o Montagem de Consulta do Banco de Dados
- routeone - Facilita a Montagem de Rotas
- validationone - Valida as Informações

# Descrição do Sistema
	O Sistema Visa Gerenciar o Cadastro de Estoque de uma Loja, nele é possível Incluir, Alterar, Remover e Visualizar Produtos e Categorias.
	O Sistema ainda salva em log no banco de dados, cada passo em que o usuário irá realizar no sistema,
	desde a entrada no sistema até a remoção de um produto.

# Como Utilizar
	Inicialmente é necessário ter instalado o PHP, MySQL na máquina;
	Após devemos criar um banco para nossos dados, logo após criar o banco,
-	Edite o arquivo **configs/db_credentials.php** com as configurações do seu banco;
- Abra o terminal de seu Sistema Operacional(SO);
- Vá até o diretório raiz do projeto
- digite o comando:  ```php optionals\create_db_tables.php```
- Teremos o retorno abaixo, confirmando que foi inserido as tabelas

> Product Table Created in Database! <br>
Category Table Created in Database! <br>
Product Category Table Created in Database! <br>
Log Table Created in Database! <br>

-	Após inserirmos as tabelas, podemos subir nosso projeto¹.

#### Preenchendo de dados
	No projeto existe um script onde realiza o preenchimento de dados no banco para teste, para realizar esta proeza faça:
-	Tenha um Arquivo com o formato CSV Chamado **import**²;
-	Salve o arquivo na pasta **optionals** localizado no projeto;
-	Execute o comando ```php optionals\seed_import.php```;
-	Aguarde obter o resultado de Total de Dados Foram Preenchidos, após a finalização nosso sistema terá seus dados;
	 

>¹ O Projeto foi feito no Windows, portanto as configurações foram efetuadas ´para utilização no Windows com Xampp; <Br>
² O Arquivo deverá ter o cabeçalho no formato pré estabelecido, sendo ele **nome;sku;descricao;quantidade;preco;categoria**, o delimitador pode ser a sua escolha, desde que altere na variável, ```$delimiter``` contida no script **seed_import.php** seja alterada para a sua customização. <br>

# Problemas e Soluções
0. [**PROBLEMA**] Pegar Produto com mais de uma Categoria<br>
	Para resolver este quebra cabeça foi realizado uma consulta específica no banco de dados, criando uma função no **dao/ProductDao** onde seleciona os dados de categoria e produtos via a 
	tabela de relacionamento **ProductCategory**, e com os dados obtidos é formado um array onde realizamos uma varredura para pegarmos as categorias do Produto. <br> 
	É utilizado um array com a chave sendo o ID do Produto, assim ao obtermos um produto com mais de uma categoria é verificado se ele já foi inserido, caso já tenha ele trata o envio dos dados para a View, evitando vir Produtos repetidos para cada Categoria associada.<br>
1. [**PROBLEMA**] Economizar recursos para Editar Produto <br>
	Para evitar codificar mais, gerando assim mais utilização de recursos do sistema, editar a categoria do produto, foi optado por excluir a associação e criar uma nova, simplificando a lógica.