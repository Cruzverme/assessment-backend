@extends('layout/header.blade.php')
@section('content')
  <!-- Main Content -->
  <main class="content">
    
    @if (@$category['name'] != NULL)
      <h1 class="title new-item">Edit Category {{$category['code']}}</h1>
      <form action="../../Category/Update" >
      <?php $back_path = "../../Product/List"; ?>
    @else
      <h1 class="title new-item">New Category</h1>
      <form action="Store" method="post">
      <?php $back_path = "../Product/List"; ?>
    @endif
        @csrf()
        <div class="input-field">
          <label for="category-name" class="label">Category Name</label>
          <input type="text" name="name" class="input-text" value="{{$category['name']}}" required>
          @if(valid()->getMessageId('name')->countError())
            <div class="text-danger">{{valid()->getMessageId('name')->first()}}<br></div>
          @endif()

        </div>
        
        <input type="hidden" name="code" class="input-text" value="{{$category['code']}}">
        <div class="actions-form">
          <a href="{{$back_path}}" class="action back">Back</a>
          <input class="btn-submit btn-action"  type="submit" value="Save" />
        </div>
      </form>
    </main>
  <!-- Main Content -->
@stop