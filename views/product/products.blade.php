
@extends('layout/header.blade.php')

@section('content')
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="../Product/New" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>

      @foreach($products as $product)
        <tr class="data-row">
          <td class="data-grid-td">
            <span class="data-grid-cell-content">{{$product['name']}}</span>
          </td>
        
          <td class="data-grid-td">
            <span class="data-grid-cell-content">{{$product['SKU']}}</span>
          </td>
          
          <td class="data-grid-td">
            <span class="data-grid-cell-content">{{$product['price']}}</span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content">{{$product['quantity']}}</span>
          </td>

          <td class="data-grid-td">
            @foreach($product['category'] as $category)
              <span class="data-grid-cell-content">
                {{ $category }}<Br />
              </span>
            @endforeach
          </td>
        
          <td class="data-grid-td">
            <div class="actions">
              <div class="action edit"><a href="Edit/{{$product['product_id']}}">Edit</a></div>
              <div class="action delete"><a href="Destroy/{{$product['product_id']}}">Delete</a></div>
            </div>
          </td>
        </tr>
      @endforeach
      
    </table>
  </main>
  <!-- Main Content -->
@stop
