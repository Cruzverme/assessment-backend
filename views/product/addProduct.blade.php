@extends('layout/header.blade.php')
@section('content')
  <!-- Main Content -->
  <main class="content">
    @if($product['id'] == NULL)
      <h1 class="title new-item">New Product</h1>
      <form action="Store" method="post">
      <?php $back_path = "../Product/List"; ?>
    @else
      <h1 class="title new-item">Edit Product</h1>
      <form action="../Update">
      <?php $back_path = "../../Product/List"; ?>
    @endif
        @csrf()
        <div class="input-field">
          <label for="SKU" class="label">Product SKU</label>
          <input type="text" name="SKU" class="input-text" value="{{$product['SKU']}}" required>
          @if(valid()->getMessageId('SKU')->countError())
            <div class="text-danger">{{valid()->getMessageId('sku')->first()}}<br></div>
          @endif()
        </div>
        <input type="hidden" name="id" class="input-text" value="{{$product['id']}}">
        <div class="input-field">
          <label for="name" class="label">Product Name</label>
          <input type="text" name="name" class="input-text" value="{{$product['name']}}" required>
          @if(valid()->getMessageId('name')->countError())
            <div class="text-danger">{{valid()->getMessageId('name')->first()}}<br></div>
          @endif()
        </div>
        <div class="input-field">
          <label for="price" class="label">Price</label>
          <input type="number" name="price" class="input-text" step=".01" value="{{$product['price']}}" required>
          @if(valid()->getMessageId('price')->countError())
            <div class="text-danger">{{valid()->getMessageId('price')->first()}}<br></div>
          @endif()
        </div>
        <div class="input-field">
          <label for="quantity" class="label">Quantity</label>
          <input type="number" name="quantity" class="input-text"  value="{{$product['quantity']}}" required>
          @if(valid()->getMessageId('description')->countError())
            <div class="text-danger">{{valid()->getMessageId('description')->first()}}<br></div>
          @endif()
        </div>
        <div class="input-field">
          <label for="category" class="label">Categories</label>
          <select multiple id="category" name="categories[]" class="input-text" required>
           @foreach($categories as $category)
            <option value="{{$category['code']}}"
              @for($i=0;$i < count($productCategory);$i++) 
                @if($productCategory[$i]['code'] == $category['code'])
                  selected
                @endif
              @endfor
            >
              {{$category['name']}}
            </option>
           @endforeach
          </select>
        </div>
        <div class="input-field">
          <label for="description" class="label">Description</label>
          <textarea name="description" class="input-text" placeholder="Textarea" required>{{$product['description']}}</textarea>
          @if(valid()->getMessageId('description')->countError())
            <div class="text-danger">{{valid()->getMessageId('description')->first()}}<br></div>
          @endif()
        </div>
        <div class="input-field">
          <label for="product_image" class="label">Upload Image</label>
          <input class='input-text' type='file' name='product_image' id='product_image' />
          @if(valid()->getMessageId('description')->countError())
            <div class="text-danger">{{valid()->getMessageId('product_image')->first()}}<br></div>
          @endif()
        </div>
        <div class="actions-form">
          <a href="{{$back_path}}" class="action back">Back</a>
          <input class="btn-submit btn-action" type="submit" value="Save Product" />
        </div>
        
      </form>
    </main>
    <!-- Main Content -->
@stop