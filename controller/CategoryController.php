<?php 

  namespace app\storeManager\controller;

  use app\storeManager\dao\CategoryDao;
  use app\storeManager\factory\CategoryFactory;
  use app\storeManager\helper\Helper;
  use app\storeManager\dao\ProductCategoryDao;

  class CategoryController
  {
      public static function HomeAction($id="",$idparent="",$event="") {
        header('Location: Category/List');
      }

      public static function NewActionGet($id="",$idparent="",$event="") {
        $category=CategoryFactory::Factory(); // we create a new category.
        blade()->regenerateToken();
        echo blade()->run('category.addCategory',['category'=>$category]);
        Helper::logger("ENTROU NA PAGINA NOVO CADASTRO DE CATEGORIA");
      }

      public static function StoreActionPost($id="",$idparent="",$event="") {
        $category=CategoryFactory::Fetch(); // we read the information obtained from the user
        if (blade()->csrfIsValid()) { // we validated the token
          if (valid()->messageList->errorcount===0) {
            if (CategoryDao::insert($category)) {
              // category inserted correctly, let's go to the list
              header('Location: ../Category/List');
              Helper::logger("CATEGORIA $category[name] CADASTRADA");
              exit();
            }
          }
        } else {
          valid()->addMessage('TOKEN','Token invalid','error');
          Helper::logger("FALHA AO CADASTRAR NOVA CATEGORIA");
        }
        echo blade()->run('category.categories',['category'=>$category]);

      }

      public static function EditActionGet($id="",$idparent="",$event="") {
        $category=CategoryDao::listOne($id);
        blade()->regenerateToken();
        echo blade()->run('category.addCategory',['category'=>$category]);
        Helper::logger("ENTROU NA PAGINA EDITAR CADASTRO DE CATEGORIA $category[name]");
      }

      public static function UpdateAction($id="",$idparent="",$event="") {
        $category=CategoryFactory::FetchUpdate(); // we read the information obtained from the user
        if (blade()->csrfIsValid()) { // we validated the token
          if (valid()->messageList->errorcount===0) {
            if (CategoryDao::update($category)) {
              // category inserted correctly, let's go to the list
              header('Location: ../Category/List');
              Helper::logger("CATEGORIA $category[name] EDITADA");
              exit();
            }
          }
        } else {
          valid()->addMessage('TOKEN','Token invalid','error');
          Helper::logger("FALHA AO EDITAR NOVA CATEGORIA");
        }
        echo blade()->run('category.categories',['category'=>$category]);
      }

      public static function DestroyAction($id="",$idparent="",$event="") {
        if(ProductCategoryDao::deleteByCategory($id) && CategoryDao::delete($id))
        {
          header('Location: ../List');
          Helper::logger("CATEGORIA DE ID $id REMOVIDA");
          exit();
        }else{
          header('Location: ../List');
          Helper::logger("CATEGORIA DE ID $id NÃO REMOVIDA");
          exit();
        }
      }

      public static function ListAction($id="",$idparent="",$event="") {
        $categories=CategoryDao::list();
        echo blade()->run('category.categories',['categories'=>$categories]);
        Helper::logger("ENTROU NA PAGINA INICIAL DE CATEGORIA");
      }

      
  }