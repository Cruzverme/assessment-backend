<?php 

  namespace app\storeManager\controller;

  use app\storeManager\dao\ProductDao;
  use app\storeManager\factory\ProductFactory;
  use app\storeManager\dao\CategoryDao;
  use app\storeManager\dao\ProductCategoryDao;
  use app\storeManager\helper\Helper;

  class ProductController
  {
    public static function HomeAction($id="",$idparent="",$event="") {
      header('Location: Product/List');
    }

    ##### CREATE PRODUCT ACTIONS ####
    public static function NewActionGet($id="",$idparent="",$event="") {
      $product=ProductFactory::Factory(); // we create a new product.
      $categories=CategoryDao::list(); // List categories
      blade()->regenerateToken();
      echo blade()->run('product.addProduct',['product'=>$product, 'categories'=>$categories, 'productCategory'=>array()]);
      Helper::logger("ENTROU NA PAGINA NOVO CADASTRO DE PRIDUTO");
    }

    public static function StoreActionPost($id="",$idparent="",$event="") {
      $product=ProductFactory::Fetch(); // we read the information obtained from the user
      if (blade()->csrfIsValid()) { // we validated the token
        if (valid()->messageList->errorcount===0) {
          if (ProductDao::insert($product)) {
            $lastProduct=ProductDao::listLast(); // get the last product
            foreach ($product['categories[]'] as $category)
            {  
              ProductCategoryDao::insert($lastProduct,$category);
            } 
            // product inserted correctly, let's go to the list
            header('Location: ../Product/List');
            Helper::logger("PRODUTO $product[name] CADASTRADA");
            exit();
          }
        }
      } else {
        valid()->addMessage('TOKEN','Token invalid','error');
        Helper::logger("FALHA AO CADASTRAR NOVO PRODUTO");
      }
      echo blade()->run('product.products',['product'=>$product]);

    }
    ##### END CREATE PRODUCT ACTIONS ####

    ##### UPDATE PRODUCT ACTIONS ####
    public static function EditActionGet($id="",$idparent="",$event="") {
      $product=ProductDao::listOne($id);
      $categories=CategoryDao::list(); // List categories
      $productCategory = ProductCategoryDao::listProductCategories($product['id']);
      blade()->regenerateToken();
      echo blade()->run('product.addProduct',['product'=>$product,'categories'=>$categories,'productCategory'=>$productCategory]);
      Helper::logger("ENTROU NA PAGINA EDITAR CADASTRO DE PRODUTO $product[name]");
    }

    public static function UpdateAction($id="",$idparent="",$event="") {
      $product=ProductFactory::FetchUpdate(); // we read the information obtained from the user
      if (blade()->csrfIsValid()) { // we validated the token
        if (valid()->messageList->errorcount===0) {
          if (ProductDao::update($product)) {
            if(ProductCategoryDao::delete($product['id']))
            {
              foreach ($product['categories[]'] as $category)
              {  
                ProductCategoryDao::insert($product,$category);
              }
            }
            
            // product inserted correctly, let's go to the list
            header('Location: ../Product/List');
            Helper::logger("CATEGORIA $product[name] EDITADA");
            exit();
          }
        }
      } else {
        valid()->addMessage('TOKEN','Token invalid','error');
        Helper::logger("FALHA AO EDITAR NOVO PRODUTO");
      }
      echo blade()->run('product.products',['product'=>$product]);
    }
    ##### END UPDATE PRODUCT ACTIONS ####
    
    ##### DESTROY PRODUCT ACTION ####

    public static function DestroyAction($id="",$idparent="",$event="") {
      if(ProductCategoryDao::delete($id))
      {
        if (ProductDao::delete($id)) {
          header('Location: ../List');
          Helper::logger("PRODUTO DE ID $id REMOVIDO");
          exit();
        }
      }
      
    }
    ##### END DESTROY PRODUCT ACTIONS ####

    ##### LIST PRODUCTS ACTION ####
    public static function ListAction($id="",$idparent="",$event="") {
      $productsWithCategories[] = [];
      $arrayPassado = array();
      $category_atual = NULL;

      $products=ProductDao::list();
      foreach($products as $product)
      {
        $categories = ProductCategoryDao::listByCategory($product['id']);
        
        foreach($categories as $category)
        {
          $productID = $category['product_id']; // get Product ID Associate in Categories

          if(in_array($productID,$arrayPassado))
          { 
            array_push($productsWithCategories[$productID]['category'],$category['category']);
          }else{
            $productsWithCategories[$productID] = $category; // ele cria o product with category
            $productsWithCategories[$productID]['category'] = array($productsWithCategories[$productID]['category']); //transforma categoria em array
            array_push($arrayPassado,$category['product_id']);
          }
        }
      }
      #remove valores nulos
      $productsWithCategories = array_filter($productsWithCategories);

      echo blade()->run('product.products',['products'=>$productsWithCategories]);
      Helper::logger("ENTROU NA PAGINA INICIAL DE PRODUTO");
    }
  }