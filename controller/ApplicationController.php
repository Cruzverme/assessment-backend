<?php 

  namespace app\storeManager\controller;

  use app\storeManager\dao\ProductDao;
  use app\storeManager\factory\ProductFactory;
  use app\storeManager\helper\Helper;


  class ApplicationController
  {
      public static function HomeAction($id="",$idparent="",$event="") {
        $products=ProductDao::listOnly4();
        echo blade()->run('dashboard',['products'=>$products]);
        Helper::logger("ENTROU NA PAGINA INICIAL");
      }
  }
